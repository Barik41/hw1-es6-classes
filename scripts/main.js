class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name; 
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age; 
    }
    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary; 
    }
    set salary(value) {
        this._salary = value;
    }
}

const empl = new Employee("Steve", 11, 1111);
console.log(empl)
console.log(empl.age)

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang; 
    }
    set lang(value) {
        this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const FirstProgrammer = new Programmer('Mike', 22, 2222, 'english');
const SecondProgrammer = new Programmer('John', 33, 3333, 'english');
const ThirdProgrammer = new Programmer('Carry', 44, 4444, 'english');

console.log(FirstProgrammer.name);
console.log(SecondProgrammer);
console.log(ThirdProgrammer);

